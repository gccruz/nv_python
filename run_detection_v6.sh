#!/usr/bin/env bash

#MDL="../../../models/detectnet_v6/snapshot_iter_24045.caffemodel"
#DPL="../../../models/detectnet_v6/deploy_h10_e2.prototxt"
#MEAN="../../../models/detectnet_v6/mean.binaryproto"
#SFX="v4_rect1_v5_5e3_mult"

# Traditional sequences used for TCSVT
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-28-36_jai_eo_.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/GP030175_part01.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/bigShipHighAlt_clip2.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

#VDO="/home/gcx/gccruz_academiafa/workspace/CFT_OTB/data_seq/jai_eo_2.mp4"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;



#MDL="/home/gcx/gccruz_academiafa/paper/new_paper/networks/detectnet_v8/snapshot_iter_31392.caffemodel"
#DPL="/home/gcx/gccruz_academiafa/paper/new_paper/networks/detectnet_v8/deploy_h10_e2.prototxt"
#MEAN="/home/gcx/gccruz_academiafa/paper/new_paper/networks/detectnet_v8/mean.binaryproto"
MDL="../../../models/detectnet_v6/snapshot_iter_24045.caffemodel"
DPL="../../../models/detectnet_v6/deploy_h10_e2.prototxt"
MEAN="../../../models/detectnet_v6/mean.binaryproto"
SFX="v6_oceans"

#VDO="/home/gcx/gccruz_academiafa/workspace/CFT_OTB/data_seq/jai_eo_4.mp4"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

#VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

#VDO="/home/gcx/datasets/videos_to_test/bigShipHighAlt_clip2.avi"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-28-36_jai_eo_.avi"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-05-15_jai_eo.mp4"
#echo "Running $VDO$"
#python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;


#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull_videos_labeled/2014-04_portimao/videos/GP030175_part01.avi"
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull_videos_labeled/2014-04_portimao/videos/GP030175_part01.avi"
VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-05-15_jai_eo_.avi"
echo "Running $VDO$"
python detect_orig_lay_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;