#!/usr/bin/env bash

echo "The folder to use is  $1 "

echo $1
ls $1/images

# This script load the result file (e.g. bigShipHighAlt_clip2result.txt) and the relevant video clip and produces images with bounding boxes drawn on those images
# Additionally, goes to the folder containing the images and creates a video clip based on those images
# At last, deletes the images

python loadVideoResults.py ~/Dropbox/workspaces/matlab/datasets/videos_to_test/bigShipHighAlt_clip2.avi   $1/bigShipHighAlt_clip2result.txt $1/images/
avconv -r 25 -q:v 2 -i $1/images/%08d.jpg $1/jai_15000.mp4
rm "$1"/images/*.jpg
