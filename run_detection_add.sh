#!/usr/bin/env bash

MDL="../../../models/detectnet_v6/snapshot_iter_24045.caffemodel"
DPL="../../../models/detectnet_v6/deploy_h10_e2.prototxt"
MEAN="../../../models/detectnet_v6/mean.binaryproto"
SFX="ocenas_add_test"


#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-28-36_jai_eo_.avi"
#echo "Running $VDO$"
#python detect_add_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;

#VDO="/home/gcx/datasets/videos_to_test/GP030175_part01.avi"
#echo "Running $VDO$"
#python detect_add_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;

VDO="/home/gcx/datasets/videos_to_test/bigShipHighAlt_clip2.avi"
echo "Running $VDO$"
python detect_add_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
echo "Running $VDO$"
python detect_add_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

