##### Image resizing
This example resizes images using the `Squash` method.
In DIGITS, the same method will be used as was originally used when creating your dataset (`Crop`, `Squash`, `Fill` or `HalfCrop`).

##### Mean subtraction
This example subtracts a mean pixel rather than the whole mean file.
In DIGITS, the same method will be used as was originally used when training your model (`None`, `Image` or `Pixel`).

## Requirements
See [BuildCaffe.md](../../docs/BuildCaffe.md) for instructions about installing caffe.
Other requirements can be found in `requirements.txt`.
You do not need as many packages to run this example as you do to run DIGITS.

#### Run detection example with frame skipping, mht tracking and modified layer

To run detection example while skipping every other frame,  tracking for a given time interval and using a modified layer
```
python detect_example_skipper.py path/to/models/detectnet_vXX/snapshot_iter_XYZ.caffemodel 
../../../models/detectnet_v3/deploy_changed9.prototxt  
/paths/to/videos_to_test/lanchaArgos_clip3.avi  
-m  ../../../models/detectnet_v3/mean.binaryproto 
-gpu 1 -tracking_duration 6 -sufix eps20bb229
```


**GENERAL**

The file included in this folder were created from the detection example provided in DIGITS.  
The networks used, are based on detectnet.


**oceans' submission 20/03/2017**
The script that was used was _run_detection_fdback_v6_track_plots.sh_ 
This called a python script with the name of detectnet fdback_skipper_track_plot.py

This example not only saved the results in each instant of time as well as constructed tracks, 
enforced the creation of missing detections 