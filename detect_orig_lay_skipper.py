#!/usr/bin/env python2
import argparse
import os
import cv2
import time
import json
from scipy.ndimage.filters import gaussian_filter
from google.protobuf import text_format
import numpy as np
from sklearn.cluster import AffinityPropagation
os.environ['GLOG_minloglevel'] = '2' # Suppress most caffe output
import caffe
from caffe.proto import caffe_pb2


# Rectangles overlap start
class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Rect(object):
    def __init__(self, p1, p2, _score):
        '''Store the top, bottom, left and right values for points
               p1 and p2 are the (corners) in either order
        '''
        self.left   = min(p1.x, p2.x)
        self.right  = max(p1.x, p2.x)
        self.bottom = max(p1.y, p2.y)
        self.top    = min(p1.y, p2.y)
        self.score = _score

    def __str__(self):
         return "Rect[%d, %d, %d, %d, %d]" % ( self.left, self.top, self.right, self.bottom, self.score )


"""
Classify an image using individual model files

Use this script as an example to build your own tool
"""


def get_net(caffemodel, deploy_file, gpu_to_use, use_gpu=True):
    """
    Returns an instance of caffe.Net

    Arguments:
    caffemodel -- path to a .caffemodel file
    deploy_file -- path to a .prototxt file

    Keyword arguments:
    use_gpu -- if True, use the GPU for inference
    """
    if use_gpu:
        caffe.set_mode_gpu()
        caffe.set_device(gpu_to_use)

    # load a new model
    return caffe.Net(deploy_file, caffemodel, caffe.TEST)


def get_transformer(deploy_file, mean_file):
    """
    Returns an instance of caffe.io.Transformer

    Arguments:
    deploy_file -- path to a .prototxt file

    Keyword arguments:
    mean_file -- path to a .binaryproto file (optional)
    """
    network = caffe_pb2.NetParameter()
    with open(deploy_file) as infile:
        text_format.Merge(infile.read(), network)

    if network.input_shape:
        dims = network.input_shape[0].dim
    else:
        dims = network.input_dim[:4]

    t = caffe.io.Transformer(
            inputs = {'data': dims}
            )
    t.set_transpose('data', (2,0,1)) # transpose to (channels, height, width)

    # color images
    if dims[1] == 3:
        # channel swap
        t.set_channel_swap('data', (2,1,0))

    if mean_file:

        print('Getting mean file')

        # set mean pixel
        with open(mean_file,'rb') as infile:
            blob = caffe_pb2.BlobProto()
            blob.MergeFromString(infile.read())
            if blob.HasField('shape'):
                blob_dims = blob.shape
                assert len(blob_dims) == 4, 'Shape should have 4 dimensions - shape is "%s"' % blob.shape
            elif blob.HasField('num') and blob.HasField('channels') and \
                    blob.HasField('height') and blob.HasField('width'):
                blob_dims = (blob.num, blob.channels, blob.height, blob.width)
            else:
                raise ValueError('blob does not provide shape or 4d dimensions')
            pixel = np.reshape(blob.data, blob_dims[1:]).mean(1).mean(1)
            t.set_mean('data', pixel)

    return t


def forward_pass(images, net, transformer, batch_size=None):
    """
    Returns scores for each image as an np.ndarray (nImages x nClasses)

    Arguments:
    images -- a list of np.ndarrays
    net -- a caffe.Net
    transformer -- a caffe.io.Transformer

    Keyword arguments:
    batch_size -- how many images can be processed at once
        (a high value may result in out-of-memory errors)
    """
    if batch_size is None:
        batch_size = 1

    caffe_images = []
    for image in images:
        if image.ndim == 2:
            caffe_images.append(image[:,:,np.newaxis])
        else:
            caffe_images.append(image)

    dims = transformer.inputs['data'][1:]

    scores = None
    for chunk in [caffe_images[x:x+batch_size] for x in xrange(0, len(caffe_images), batch_size)]:

        new_shape = (len(chunk),) + tuple(dims)
        if net.blobs['data'].data.shape != new_shape:
            net.blobs['data'].reshape(*new_shape)
        for index, image in enumerate(chunk):
            # transpose image with opencv
            sec_time_start = time.time()
            image_copy = np.swapaxes(np.swapaxes(image, 2, 0), 1, 2)
            sec_time_end = time.time()
            print('transpose time: ', sec_time_end-sec_time_start)
            image_data = transformer.preprocess('data', image) # TODO This is taking a lot of time (0.1 seconds)
            net.blobs['data'].data[index] = image_data # image_copy # image_data
        start = time.time()
        output = net.forward()[net.outputs[-1]]
        end = time.time()

        if scores is None:
            scores = np.copy(output)
        else:
            scores = np.vstack((scores, output))
        print 'Processed %s/%s images in %f seconds (forward pass call)' % (len(scores), len(caffe_images), (end - start))
        # print scores
    return scores


def coverage_pass(images, feedback_map, net, transformer, batch_size=None):
    """
    Returns scores for each image as an np.ndarray (nImages x nClasses)

    Arguments:
    images -- a list of np.ndarrays
    net -- a caffe.Net
    transformer -- a caffe.io.Transformer

    Keyword arguments:
    batch_size -- how many images can be processed at once
        (a high value may result in out-of-memory errors)
    """
    if batch_size is None:
        batch_size = 1

    caffe_images = []
    for image in images:
        if image.ndim == 2:
            caffe_images.append(image[:,:,np.newaxis])
        else:
            caffe_images.append(image)

    dims = transformer.inputs['data'][1:]

    scores = None
    for chunk in [caffe_images[x:x+batch_size] for x in xrange(0, len(caffe_images), batch_size)]:

        new_shape = (len(chunk),) + tuple(dims)
        if net.blobs['data'].data.shape != new_shape:
            net.blobs['data'].reshape(*new_shape)
        for index, image in enumerate(chunk):
            # transpose image with opencv
            image_data = transformer.preprocess('data', image)  # TODO This is taking a lot of time (0.1 seconds)
            net.blobs['data'].data[index] = image_data  # image_copy # image_data

        # get coverage
        coverage = net.blobs['coverage'].data

        increasing_map = np.zeros((1,1,feedback_map.shape[0], feedback_map.shape[1]))

        # feedback_map = cv2.dilate(feedback_map, np.ones((3,3), np.uint8))
        #feedback_map = np.float32(feedback_map)
        # blurred = gaussian_filter(feedback_map, sigma=3)
        # cv2.imshow('increased', blurred)

        increasing_map[0, 0, :, :] = np.clip(0.1 * feedback_map, 0, 0.05)
        # increasing_map[0, 0, :, :] = 1.05 * blurred

        print('increasing map', np.max(increasing_map))
        print('max(coverage)', np.max(coverage))

        cv2.imshow('cov0', coverage[0, 0, :, :])

        # coverage = np.multiply(coverage, increasing_map)
        coverage = coverage + increasing_map
        print('max(coverage)', np.max(coverage))
        print('coverage', np.shape(coverage))

        coverage = np.clip((coverage ), 0, 1.0)
        cv2.imshow('cov_final', coverage[0, 0, :, :])
        cv2.waitKey(0)

        # put coverage again into the network
        for index, image in enumerate(chunk):
            net.blobs['coverage'].data[index] = coverage

        # get bb's list
        output_after_cov = net.forward(start='cluster')[net.outputs[-1]]

        if scores is None:
            scores = np.copy(output_after_cov)
        else:
            scores = np.vstack((scores, output_after_cov))

    print 'Got into coverage pass'
    print('scores[:5,:]', scores[:5,:])
    return scores


def distance_from_location(det1, det2 ):
    """ Compute  distance from a location"""
    return (((det1[0]- det2[0])** 2) + ((det1[1]- det2[1]) ** 2) + ((det1[2]- det2[2]) ** 2) + ((det1[3]- det2[3]) ** 2)) ** 0.5


def compute_area(r1, r2):  # returns None if rectangles don't intersect

    dx = min(r1.right, r2.right) - max(r1.left, r2.left)
    dy = min(r1.bottom, r2.bottom) - max(r1.top, r2.top)

    if (dx >= 0) and (dy >= 0):
        _area = dx*dy
    else:
        _area = 0

    #  Smallest area rectangle
    a_r1 = (r1.right-r1.left) * (r1.bottom - r1.top)
    a_r2 = (r2.right - r2.left) * (r2.bottom - r2.top)

    if a_r1 == 0 or a_r2 ==0:
        relative_area = 0
    else:
        relative_area = float(_area) / float(min(a_r1,a_r2))
    return relative_area


def range_overlap(a_min, a_max, b_min, b_max):
    '''Neither range is completely greater than the other
    '''
    return (a_min <= b_max) and (b_min <= a_max)


def rect_overlaps(r1,r2):
    # print range_overlap(r1.left, r1.right, r2.left, r2.right)
    # print r1.left, r1.right, r2.left, r2.right
    # print range_overlap(r1.top, r1.bottom, r2.top, r2.bottom)
    # print r1.top, r1.bottom, r2.top, r2.bottom
    return range_overlap(r1.left, r1.right, r2.left, r2.right) and range_overlap(r1.top, r1.bottom, r2.top, r2.bottom)


def merge_rectangles(_rectangles):
    _clusters = []
    _area_clusters = []

    for rect in _rectangles:
        matched = 0

        for _area_cluster in _area_clusters:
            area = compute_area(rect, _area_cluster)
            if area > 0.1:
                matched = 1

                max_index = np.argmax([_area_cluster.score, rect.score])

                if max_index == 0:
                    _area_cluster.left = int(_area_cluster.left)
                    _area_cluster.right = int(_area_cluster.right)
                    _area_cluster.top = int(_area_cluster.top)
                    _area_cluster.bottom = int(_area_cluster.bottom)
                    _area_cluster.score = _area_cluster.score + rect.score

                else:
                    # Keep the BB with the highest score
                    _area_cluster.left = int(rect.left)
                    _area_cluster.right = int( rect.right)
                    _area_cluster.top = int(rect.top)
                    _area_cluster.bottom = int(rect.bottom)
                    _area_cluster.score = _area_cluster.score + rect.score

        if (not matched) and not rect.bottom == 0 and not rect.top == 0 and not rect.right== 0 and not rect.left == 0 :
            _area_clusters.append(rect)

    # print('area clusters: ', np.shape(np.asarray(_area_clusters)))
    return _area_clusters


def fill_rect_struct(_points):
    '''
    This method constructs objects from np arrays
    Args:
        _points: receives np.arrays
    Returns:
        _rectangles: The object specified in the class Rect
    '''

    _rectangles = []
    for _ptr in range(0, _points.shape[0]):
        _rectangles.append(Rect(Point(_points[_ptr, 1], _points[_ptr, 0]), Point(_points[_ptr, 3], _points[_ptr, 2]), _points[_ptr,4]))

    return _rectangles


def extract_from_struct(_rectangle):

    _points = np.zeros((_rectangle.__len__(), 4))
    _scores = np.zeros((_rectangle.__len__(), 1))
    _ptr = 0
    for _rect in _rectangle:
        _points[_ptr, :] = [_rect.bottom, _rect.left, _rect.top, _rect.right]
        _scores[_ptr, :] = [_rect.score]
        _ptr += 1

    return _points, _scores
# Rectangles overlap end


# Find all paths between two points in the Graph
def get_all_paths(_graph, start, end, _path=[]):
    _path = _path + [start]

    if start == end:
        return [_path]
    if not _graph.has_key(start):
        return []
    paths = []
    for node in _graph[start]:
        if node not in _path:
            if type(node) == int:
                newpaths = get_all_paths(_graph, node, end, _path)
            else:
                pass
            for newpath in newpaths:
                paths.append(newpath)
    return paths


# compute the multiplication of scores along Graph
def compute_detection_score(_graph, _path):
    """
    Args:
        _graph: graph containing the elements
        _path: path in which one wants to compute the score

    Returns:
        _score: consists on multiplication of the scores along the path
    """
    _score = 1.0
    for _ptr in range(0, _path.shape[0]):
        _score = float(_score * float(_graph[_path[_ptr]][-2]['score']))
        # print('intermediate_score', _score)
        # print type(_score)

    _score = 1 /(1 + np.exp(-((3 * _score) - 4)))
    # print('_score', _score)
    return _score


# compute the multiplication of scores along Graph
# def compute_additive_detection_score(_graph, _path):
#     """
#     Args:
#         _graph: graph containing the elements
#         _path: path in which one wants to compute the score
#
#     Returns:
#         _score: consists on multiplication of the scores along the path
#     """
#     _score = 0.0
#     for _ptr in range(0, _path.shape[0]):
#         print _graph[_path[_ptr]][-2]['score']
#
#         _score = float(_score + float(_graph[_path[_ptr]][-2]['score']))
#         # 1 / (1 + np.exp(-((3 * _score) - 4)))
#         # _score_changed = float(_score * float(_graph[_path[_ptr]][-2]['score']))
#
#     # _score = 1 /(1 + np.exp(-((3 * _score) - 4)))
#     # print('_score', _score)
#     return _score


# compute the proximity scores along the graph


def compute_proximity_score(_graph, _path, sigma_x, sigma_y, _gaussian_const):
    """
    Args:
        _graph: graph containing the elements
        _path: path in which one wants to compute the score
        sigma_x: parameter for the gaussian decay function (X)
        sigma_y: parameter for the gaussian decay function (Y)
        _gaussian_const: normalizing constant
    Returns:
        _similarity_score: score
    """
    _distance_score = 0.0
    for _ptr in range(1, _path.shape[0]):
        distance_x = (_graph[_path[_ptr]][-3]['position'][0] - _graph[_path[_ptr - 1]][-3]['position'][0])
        distance_y = (_graph[_path[_ptr]][-3]['position'][1] - _graph[_path[_ptr - 1]][-3]['position'][1])
        # gaussian_weight = (_gaussian_const / (2.0 * np.pi * sigma_x * sigma_y) * np.exp(-((distance_x ** 2 + distance_y ** 2) / (2.0 * sigma_y * sigma_x))))
        gaussian_weight =  np.exp(-((distance_x ** 2 + distance_y ** 2) / (2.0 * sigma_y * sigma_x)))
        _distance_score += gaussian_weight
    return _distance_score


# Eliminate duplicate paths between two points
def unique(a):
    """
    Args:
        a: Matrix to remove  duplicate elements
    Returns:
        a[ui]: Matrix a without repeated elements
    """
    if np.size(a) == 0:
        # print 'returning path'
        return a
    else:
        a=np.asarray(a)
        order = np.lexsort(a.T)
        a = a[order]
        diff = np.diff(a, axis=0)
        ui = np.ones(len(a), 'bool')
        ui[1:] = (diff != 0).any(axis=1)
        return a[ui]


# Get all the nodes in a given node
def get_points_in_level(_graph, _level):
    """
    Args:
        _graph:
        _level: Level to output the values

    Returns: All the nodes in a given level
    """
    _list = []
    iter_keys = _graph.iterkeys()
    for ptr in iter_keys:
        if _graph[ptr][-4]['level'] == _level:
            _list.append(ptr)

    return _list


# add one more level to the graph
def get_newest_level(_graph):
    """
    Args:
        _graph:
    Returns:
        last_level:
    """
    _last_level_ = 0
    iter_keys = _graph.iterkeys()

    for ptr in iter_keys:
        if _graph[ptr][-4]['level'] > _last_level_:  # and not np.size(_points) == 0:
            _last_level_ = _graph[ptr][-4]['level']

    return _last_level_


def add_one_level(_graph, _x_observed, _scores, _bb_observed, _detection_number, _proximity_threshold):
    """
    Args:
        _graph:
        _x_observed:
        _scores
        _detection_number:
        _proximity_threshold:
    Returns:
        _graph: with added level
        _detection_number: increased
    """
    # verify which is the last level
    _last_level = get_newest_level(_graph)# get parents
    parents_points = get_points_in_level(_graph, _last_level)

    for _ptr in range(0,_x_observed.shape[0]):
        _detection_number += 1
        _graph[_detection_number] = []
        if parents_points == []:
            _graph[_detection_number].append(0)
        else:
            spontaneous_generation = True
            for _parents_ptr in range(0, np.shape(parents_points)[0]):
                #  Add condition to verify the distance from child to possible parents
                _x_distance = np.absolute(_graph[parents_points[_parents_ptr]][-3]['position'][0] - _x_observed[_ptr, 0])
                _y_distance = np.absolute(_graph[parents_points[_parents_ptr]][-3]['position'][1] - _x_observed[_ptr, 1])

                if _x_distance > _proximity_threshold or _y_distance > _proximity_threshold:
                    pass
                else:
                    _graph[_detection_number].append(parents_points[_parents_ptr])
                    spontaneous_generation = False

            if spontaneous_generation:
                _graph[_detection_number].append(0)

        _graph[_detection_number].append({'level': _last_level+1})
        _graph[_detection_number].append({'position': [_x_observed[_ptr, 0], _x_observed[_ptr, 1]]})
        _graph[_detection_number].append({'score': 1 / (1 + np.exp(-((4 * _scores[_ptr]) - 4)))})

        _graph[_detection_number].append({'bb': [int(_bb_observed[_ptr, 0]), int(_bb_observed[_ptr, 1]), int(_bb_observed[_ptr, 2]), int(_bb_observed[_ptr, 3])]})

    return _graph, _detection_number


def get_level_count(_graph):
    """
    Args:
        _graph:
    Returns:
    """
    _last_level = get_newest_level(_graph)
    _number_of_levels =0
    for level_ptr in range(0, _last_level + 1):

        if not np.size(np.asarray(get_points_in_level(_graph, level_ptr))) == 0:
            _number_of_levels += 1

    return _number_of_levels


def print_by_levels(_graph):
    """
    Args:
        _graph:
    Returns:
        None
    """
    _last_level = get_newest_level(_graph)

    for level_ptr in range(0, _last_level+1):
        print('level: ', level_ptr)
        print(get_points_in_level(_graph, level_ptr))


def print_entire_level(_graph, _level):
    """
    Args:
        _graph:
        _level:
    Returns:
        None
    """
    _last_level = get_newest_level(_graph)

    for _level_ptr in range(0, _last_level+1):
        if _level_ptr == _level:
            print('level: ', _level_ptr)
            _points_in_level = get_points_in_level(_graph, _level_ptr)

    for _points_ptr in range(0,np.size(_points_in_level)):
        print _graph[_points_in_level[_points_ptr]]


def print_active_levels(_graph):
    """
    Args:
        _graph:
    Returns:
        None
    """
    _last_level = get_newest_level(_graph)

    for _level_ptr in range(0, _last_level+1):

        _points = get_points_in_level(_graph, _level_ptr)
        if not np.size(_points) == 0:
            print('level: ', _level_ptr)
            print _points


def get_first_active_level(_graph):
    """
    get the "oldest" level
    Args:
        _graph:
    Returns:
        _first_non_empy:
    """
    _first_non_empty = get_newest_level(_graph)
    iter_keys = _graph.iterkeys()

    for ptr in iter_keys:

        if _graph[ptr][-4]['level'] <_first_non_empty:
            _first_non_empty = _graph[ptr][-4]['level']

    return _first_non_empty


def remove_level(_graph, _level):
    """
    Args:
        _graph:
        _level:
    Returns:
        _graph. without a given level
    """
    _points_list = get_points_in_level(_graph, _level)

    # check if top level is zero or not
    if np.size( get_points_in_level(_graph, _level-1)) == 0:  # Case where level above is zero

        for points_ptr in range(0, len(_points_list)):
            _graph.pop(_points_list[points_ptr])

        if np.size(get_points_in_level(_graph, _level + 1)) == 0:  # Case where level below is zero
            pass
        else:
            next_level_points = get_points_in_level(_graph, _level + 1)
            for points_ptr in range(0, len(next_level_points)):
                temp_node = _graph[next_level_points[points_ptr]][-4:]
                _graph[next_level_points[points_ptr]]=[0, temp_node[0], temp_node[1], temp_node[2], temp_node[3]]

    else:  # case where top level is not empty

        for points_ptr in range(0, len(_points_list)):
            _graph.pop(_points_list[points_ptr])  # remove the nodes

        next_level_points = get_points_in_level(_graph, _level + 1)
        _parents_level_points = get_points_in_level(_graph, _level-1)
        for points_ptr in range(0, len(next_level_points)):  # for all the points in the child level
            temp_node = _graph[next_level_points[points_ptr]][-4:]
            _graph[next_level_points[points_ptr]] = []
            for _parents_ptr in range(0, np.shape(_parents_level_points)[0]):  # make them depend on all of its grandparents
                _graph[next_level_points[points_ptr]].append(_parents_level_points[_parents_ptr])
            _graph[next_level_points[points_ptr]].append(temp_node[0])
            _graph[next_level_points[points_ptr]].append(temp_node[1])
            _graph[next_level_points[points_ptr]].append(temp_node[2])
            _graph[next_level_points[points_ptr]].append(temp_node[3])

    return _graph


def cluster_tracks(_tracks):
    """
    Cluster the tracks_avg_loc_vel
    Args:
        _tracks:
    Returns:
        _af
    """
    _tracks = np.asarray(_tracks)

    # _af=AffinityPropagation(preference=-50, convergence_iter=40).fit(_tracks)
    _af = AffinityPropagation(convergence_iter=40).fit(_tracks)

    _clusters_centers = _af.cluster_centers_indices_

    if not _clusters_centers is None:
        _n_clusters = len(_clusters_centers)
    else:
        _n_clusters = 0
    print('_n_clusters', _n_clusters)

    return _af, _clusters_centers
# MHT End


def detect(caffemodel, deploy_file, video_file, mean_file, use_gpu, gpu_to_use, sufix, tracking_duration, _json_parameters, starting_frame, labels_file=None, batch_size=None):
    """
    Classify some images against a Caffe model and print the results

    Arguments:
    caffemodel -- path to a .caffemodel
    deploy_file -- path to a .prototxt
    video_file -- video to be processed
    gpu_to_use -- which GPU to use

    Keyword arguments:
    mean_file -- path to a .binaryproto
    labels_file path to a .txt file
    use_gpu -- if True, run inference on the GPU
    """

    sufix = str(sufix)+ str(tracking_duration)

    # Parameter for gaussian decay
    gaussian_score_sigma_x = _json_parameters['other']['gaussian_sigma_x']
    gaussian_score_sigma_y = _json_parameters['other']['gaussian_sigma_y']
    gaussian_const = 2 * np.pi * gaussian_score_sigma_x * gaussian_score_sigma_y

    edge_epsilon = _json_parameters['other']['edge_thresh']
    min_bb_size = _json_parameters['other']['minimum_size']

    # Parameters for MHT
    # tracking_duration = 10  # iterations
    proximity_threshold = int(_json_parameters['mht']['proximity_thresh'])

    # graph data
    detection_number = 0  # detection_number
    graph = {}
    old_composite_score = 0

    # performance computation
    graph_time = 0
    cnn_time = 0

    # Load the model and images
    net = get_net(caffemodel, deploy_file, gpu_to_use, use_gpu, )
    # TODO do not pass mean file
    transformer = get_transformer(deploy_file, mean_file)
    _, channels, height, width = transformer.inputs['data']
    if channels == 3:
        mode = 'RGB'
    elif channels == 1:
        mode = 'L'
    else:
        raise ValueError('Invalid number for channels: %s' % channels)

    if os.path.exists(video_file[0]):
        print('exists video file')
        # Check results file
        if os.path.exists(video_file[0][:-4] + 'result' + str(sufix) + '.txt'):  # Exist

            # if object exists but is empty
            if os.stat(video_file[0][:-4] + 'result' + str(sufix) + '.txt').st_size == 0:
                print "File exists but is empty"
                results_file = open(video_file[0][:-4] + 'result' + str(sufix) + '.txt', 'w')
                results_file_exists = False
            # not empty
            else:
                results = np.loadtxt(video_file[0][:-4] + 'result' + str(sufix) + '.txt')
                results_length = results.shape[0]

                if results_length == 0:
                    results_file = open(video_file[0][:-4] + 'result' + str(sufix) + '.txt', 'w')
                    results_file_exists = False
                else:
                    results_file_exists = True
                    results_file = open(video_file[0][:-4] + 'result' + str(sufix) + '.txt', 'a')
                    print('results file exists and is not empty. The last line is: ', results[results_length-1, :])

        # If does not exist results file
        else:
            results_file = open(video_file[0][:-4] + 'result' + str(sufix) + '.txt', 'w')
            results_file_exists = False

    else:
        print('does not exist')

    # Load video CV
    video_cap = cv2.VideoCapture(video_file[0])
    cv2.namedWindow('image', cv2.cv.CV_WINDOW_NORMAL)
    cv2.namedWindow('img_orig', cv2.cv.CV_WINDOW_NORMAL)
    cv2.namedWindow('cov0', cv2.cv.CV_WINDOW_NORMAL)
    cv2.namedWindow('cov_final', cv2.cv.CV_WINDOW_NORMAL)
    cv2.namedWindow('cov', cv2.cv.CV_WINDOW_NORMAL)
    cv2.namedWindow('increased', cv2.cv.CV_WINDOW_NORMAL)

    if results_file_exists:
        frame_idx = int(results[results_length - 1, 0]) + 1
        print('frame_idx: ', frame_idx)
        video_cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES , frame_idx)  # Desktop (if on a JETSON, it is slightly different)
    else:
        frame_idx = 0

    batch_dim = 1 # In this deployment script, is assumed that batch is always one, to decrease lag
    best_bb_1 = []

    # TODO initialize stuff for data association
    scale_x = 1
    scale_y = 1
    tracks = []
    track_to_remove = -1
    feedback_coverage = []

    while True:

        ret, image_cv = video_cap.read()
        images_cv = np.zeros((batch_dim, height, width, channels))

        # Check for end of cycle
        if (0xFF & cv2.waitKey(1) == 27) or not ret:
        #if (0xFF ) or not ret:	
            print 'Going to exit (ret equals false)'
            results_file.close()
            break

        # Resizing parameters
        orig_size = image_cv.shape
        fy = float(orig_size[0]) / float(height)
        fx = float(orig_size[1]) / float(width)

        # TODO reinitialize coverage every time
        coverage = np.zeros((48, 64))
        # TODO
        # Scale to convert to map
        fx__ = orig_size[1] / 64
        fy__ = orig_size[0] / 48

        # Resize operation
        img_orig = image_cv
        image_cv = cv2.resize(image_cv, (width, height), interpolation=cv2.INTER_LINEAR)
        images_cv[0, :, :, :] = image_cv

        if np.remainder(frame_idx, 1) == 0:
            bb_list = []
            detection_flag = False
            # Forward pass of the network
            s_time = time.time()

            # if np.asarray(feedback_coverage).size == 0:
            # scores = forward_pass(images_cv, net, transformer, batch_size=batch_size)
            #     print 'forward pass'

            scores = forward_pass(images_cv, net, transformer, batch_size=batch_size)

            # if np.asarray(feedback_coverage).size == 0 or (feedback_coverage == np.zeros_like(feedback_coverage)).all():
            #     print 'not in coverage pass'
            #     scores = forward_pass(images_cv, net, transformer, batch_size=batch_size)
            #
            # else:
            #     scores = coverage_pass(images_cv, feedback_coverage, net, transformer, batch_size=batch_size)

            t_time = time.time()
            cnn_time = cnn_time + (t_time - s_time)
            print('function call: ', (t_time - s_time))

            X = scores[0, :, :4]
            scores_temp = np.reshape(np.asarray([]), (-1, 5))

            for ptr in range(0, np.shape(X[:, :4])[0]):  # cycle through the scores' list
                # Draw BB
                cv2.rectangle(images_cv[0, :, :, :], (int(X[ptr, 0]), int(X[ptr, 1])), (int(X[ptr, 2]), int(X[ptr, 3])), [200, 0, 200])

                if scores[0, ptr, 0] > edge_epsilon and scores[0, ptr, 1] > edge_epsilon and scores[0, ptr, 2] > edge_epsilon and scores[0, ptr, 3] > edge_epsilon \
                and scores[0, ptr, 0] < width - edge_epsilon and scores[0, ptr, 1] < height - edge_epsilon \
                and scores[0, ptr, 2] < width - edge_epsilon and scores[0, ptr, 3] < height - edge_epsilon and  np.abs(scores[0, ptr, 3] - scores[0, ptr, 1]) > min_bb_size \
                and np.abs(scores[0, ptr, 2] - scores[0, ptr, 0]) > min_bb_size:
                    scores_temp = np.vstack((scores_temp, np.reshape(scores[0, ptr, :], (1,5))))
                else:
                    pass

            scores = np.zeros((1, scores_temp.shape[0], scores_temp.shape[1]))
            scores[0, :, :] = scores_temp

            # TODO probably this copy could be avoided
            X = scores[0, :, :4]

            for ptr in range(0, np.shape(X[:, :4])[0]):  # cycle through the scores' list
                cv2.rectangle(images_cv[0, :, :, :], (int(X[ptr, 0]), int(X[ptr, 1])), (int(X[ptr, 2]), int(X[ptr, 3])), [200, 0, 0])
                pass

            rectangles = fill_rect_struct(scores[0, :, :])
            merged_rectangles = merge_rectangles(rectangles)
            X, scores_after_merging = extract_from_struct(merged_rectangles)

            for ptr in range(0, np.shape(X[:, :4])[0]):  # cycle through the scores' list

                # Draw BB
                cv2.putText(images_cv[0, :, :, :], str(scores_after_merging[ptr][0])[:6], (int(X[ptr, 2]), int(X[ptr, 3])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0))
                cv2.rectangle(images_cv[0, :, :, :], (int(X[ptr, 0]), int(X[ptr, 1])), (int(X[ptr, 2]), int(X[ptr, 3])), [0, 0, 0])

            # iterate through the detections
            x_observed = []
            x_observed = np.asarray(x_observed)
            bb_observed = np.asarray([])

            for score_idx in range(0, np.shape(X)[0]):

                if not all(X[score_idx, :] == [0, 0, 0, 0]):

                    # Compute the coordinates in the original scale
                    bb_x = X[score_idx, 0] * fx
                    bb_y = X[score_idx, 1] * fy
                    bb_width = (X[score_idx, 2] - X[score_idx, 0]) * fx
                    bb_height = (X[score_idx, 3] - X[score_idx, 1]) * fy

                    x_observed = np.asarray(x_observed)
                    x_observed = np.reshape(x_observed, (-1, 3))
                    x_observed = np.concatenate((x_observed, np.reshape([int(bb_y + float(bb_height)/2.0),
                                                                         int(bb_x + float(bb_width)/2.0),
                                                                         scores[0, score_idx, 4]], [1,3])), axis=0)
                    bb_observed = np.reshape(bb_observed, (-1, 4))
                    bb_observed = np.concatenate((bb_observed, np.reshape([int(bb_y ), int(bb_x),  int(bb_height), int(bb_width)], [1,4])), axis=0)

                else:
                    pass

            # Graph
            # Add the current observations to the graph
            if not x_observed.size == 0:

                # Get number of levels
                # level_counter = get_level_count(graph)

                if old_composite_score > 0.3:
                    [graph, detection_number] = add_one_level(graph, x_observed[:, :2], x_observed[:, 2], bb_observed,
                                                              detection_number, 2 * proximity_threshold)
                else:
                    [graph, detection_number] = add_one_level(graph, x_observed[:, :2], x_observed[:, 2], bb_observed,
                                                              detection_number, proximity_threshold)

                size_of_graph = get_level_count(graph)

                # check if there are enough instants to track
                tracks_avg_loc_vel = []
                if size_of_graph >= tracking_duration:

                    start_time = time.time()
                    # Track starts here
                    newest_level_nodes = get_points_in_level(graph, get_newest_level(graph))
                    oldest_level_nodes = get_points_in_level(graph, get_first_active_level(graph))

                    for newest_level_nodes_ptr in range(0, np.size(newest_level_nodes)):  # For all the points in the last layer

                        best_path = []
                        best_score_idx = 0
                        old_composite_score = 0
                        for oldest_level_nodes_ptr in range(0, np.size(oldest_level_nodes)):  # check paths to the first non-empty level
                            # Find paths
                            unique_paths = unique(get_all_paths(graph, newest_level_nodes[newest_level_nodes_ptr], oldest_level_nodes[oldest_level_nodes_ptr]))

                            if not np.size(unique_paths) == 0:
                                # for each possible path, compute score
                                for paths_ptr in range(0, np.shape(unique_paths)[0]):
                                    # Compute scores along paths
                                    detection_score = compute_detection_score(graph, unique_paths[paths_ptr])
                                    proximity_score = compute_proximity_score(graph, unique_paths[paths_ptr], gaussian_score_sigma_x, gaussian_score_sigma_y, gaussian_const)

                                    # print('detection score', detection_score)
                                    # print('proximity score', proximity_score)
                                    composite_score = detection_score * proximity_score
                                    # print('composite_score: ', composite_score)

                                    if composite_score > old_composite_score:
                                        # Find the path with highest score
                                        best_score_idx = paths_ptr
                                        old_composite_score = composite_score
                                        best_path = unique_paths[paths_ptr]

                        if np.size(best_path) >= tracking_duration:
                            data_along_path = []

                            best_coord_1 = graph[best_path[0]][-3]['position']
                            best_bb_1 = graph[best_path[0]][-1]['bb']
                            score_string = '%.6f'% old_composite_score
                            score_string_to_file = '%.8f'% old_composite_score

                            cv2.putText(images_cv[0, :, :, :], str(score_string), (int(best_bb_1[1] / fx), int(best_bb_1[0] / fy)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))

                            # WRITE
                            results_file.write(str(int(frame_idx))+' ' +str(best_bb_1[3] + best_bb_1[1]) + ' ' + str(int(best_bb_1[0])) + ' ' + str(int(np.abs(best_bb_1[3]))) + ' ' + str(int(best_bb_1[2])) + ' ' + str(0) + ' ' + str(score_string_to_file) + '\n')
                            cv2.rectangle(images_cv[0, :, :, :], (int(best_bb_1[1] / fx), int(best_bb_1[0] / fy)),
                                          (int((best_bb_1[3] + best_bb_1[1]) / fx), int((best_bb_1[2] + best_bb_1[0]) / fy)), [0, 0, 255])

                            # TODO start data association
                            center_x = int(best_bb_1[3] + best_bb_1[1] + int(0.5 * np.abs(best_bb_1[3])))
                            center_y = int(int(best_bb_1[0]) + int(0.5 * best_bb_1[2]))
                            cv2.circle(img_orig, (center_x, center_y), 10, [0, 0, 0], 5)
                            if tracks:
                                dist = np.zeros(len(tracks))

                                for i in range(0, len(tracks)):
                                    one_track = np.reshape(tracks[i], (-1, 5))
                                    dist[i] = distance_from_location(one_track[-1, :],
                                                                     [center_y, center_x, height, width])

                                # check what is the index for the min distance
                                min_index = np.argmin(dist)
                                one_track = np.reshape(tracks[min_index], (-1, 5))
                                # if within a  accepted threshold
                                if dist[min_index] < 100:
                                    # add to tracks
                                    tracks[min_index] = np.vstack(
                                        (one_track, [center_y, center_x, height, width, frame_idx]))

                                # If too far, create new track
                                else:
                                    tracks.append([center_y, center_x, height, width, frame_idx])

                                for i in range(0, len(tracks)):
                                    track = np.reshape(tracks[i], (-1, 5))

                            else:
                                tracks.append([center_y, center_x, height, width, frame_idx])

                            for i in range(0, len(tracks)):
                                track = np.reshape(tracks[i], (-1, 5))
                                for j in range(0, track.shape[0] - 1):
                                    # plot the line
                                    cv2.line(img_orig, (track[j, 1], track[j, 0]), (track[j + 1, 1], track[j + 1, 0]),
                                             [0, 255, 0], 5)

                                # clear tracks that are already old
                                if track[-1, -1] < frame_idx - 200:
                                    track_to_remove = i

                                # Check tracks that are long enough
                                if track.shape[0] > 20 and track[-1, -1] > frame_idx - 20:
                                    print track[-1, -1]
                                    print frame_idx - 20
                                    # scale down to map
                                    scalled_center_x = int(track[-1, 1] / float(fx__))
                                    scalled_center_y = int(track[-1, 0] / float(fy__))

                                    coverage[scalled_center_y, scalled_center_x] = 1

                            # If there is a valid track to remove
                            if track_to_remove == -1:
                                pass
                            else:
                                # pop out the track
                                tracks.pop(track_to_remove)
                                track_to_remove = -1

                            # check if there is any coverage to use in the next time instant
                            if coverage.any():
                                feedback_coverage = coverage
                            else:
                                feedback_coverage = []

                            # TODO end of data association

                            # Add bounding box to a list to be replicated in the next frame
                            bb_list.append([best_bb_1[0], best_bb_1[1], best_bb_1[2], best_bb_1[3], score_string_to_file])

                            detection_flag = True

                            img_orig[int(best_bb_1[0]): 5 + int(best_bb_1[0]), int(best_bb_1[1] + best_bb_1[3]): 5 + int(best_bb_1[1] + best_bb_1[3]), :] = [0, 0, 255]
                            img_orig[int(best_bb_1[0] + best_bb_1[2]): 5 + int(best_bb_1[0] + best_bb_1[2]), int(best_bb_1[1]): 5 + int(best_bb_1[1]), :] = [0, 255, 0]

                            for nodes_in_path_ptr in range(0, np.size(best_path) - 1):
                                best_coord_0 = graph[best_path[nodes_in_path_ptr]][-3]['position']
                                best_bb_0 = graph[best_path[nodes_in_path_ptr]][-1]['bb']
                                best_coord_1 = graph[best_path[nodes_in_path_ptr + 1]][-3]['position']
                                # copy the  locations and the bb of the nodes along the best path
                                data_along_path.append([best_coord_0[0], best_coord_0[1], best_bb_0[0], best_bb_0[1], best_bb_0[2],best_bb_0[3]])

                        cv2.imshow('cov', coverage)

                    graph_time =  graph_time + (time.time() - start_time)
                    print('elapsed time (GRAPH): ', time.time() - start_time)

            else:
                newest_level = get_newest_level(graph)

            # Remove the older layers so the graph does not get to big
            while get_level_count(graph) > tracking_duration:
                remove_level(graph, get_first_active_level(graph))

            bb_np = np.reshape(np.asarray(bb_list), (-1, 5))
        else:

            if 'detection_flag' in locals():
                if not len(bb_list) == 0 and detection_flag:

                    print('scores ', bb_np.shape)
                    for bb_ptr in range(0, bb_np.shape[0]):
                        cv2.rectangle(images_cv[0, :, :, :], (int(float(bb_np[bb_ptr, 1]) / fx), int(float(bb_np[bb_ptr, 0]) / fy)),
                                      (int((float(bb_np[bb_ptr, 3]) + float(bb_np[bb_ptr, 1])) / fx), int((float(bb_np[bb_ptr, 2]) + float(bb_np[bb_ptr, 0])) / fy)), [0, 255, 255])

                        results_file.write(str(int(frame_idx)) + ' ' + str(int(bb_np[bb_ptr, 3]) + int(bb_np[bb_ptr, 1])) + ' ' + str(int(bb_np[bb_ptr, 0])) + ' ' + str(int(np.abs(float(bb_np[bb_ptr, 3])))) + ' ' + str(int(bb_np[bb_ptr, 2])) + ' ' + str(0) + ' ' + str(score_string_to_file) + '\n')

                    # cv2.rectangle(images_cv[0, :, :, :], (int(best_bb_1[1] / fx), int(best_bb_1[0] / fy)),
                    #           (int((best_bb_1[3] + best_bb_1[1]) / fx), int((best_bb_1[2] + best_bb_1[0]) / fy)), [0, 255, 255])
                    # results_file.write(str(int(frame_idx)) + ' ' + str(best_bb_1[3] + best_bb_1[1]) + ' ' + str(int(best_bb_1[0])) +
                    #                    ' ' + str(int(np.abs(best_bb_1[3]))) + ' ' + str(int(best_bb_1[2])) + ' ' + str(0) + ' ' + str(score_string_to_file) + '\n')

                    for ptr in range(0, np.shape(X[:, :4])[0]):  # cycle through the scores' list
                        cv2.rectangle(images_cv[0, :, :, :], (int(X[ptr, 0]), int(X[ptr, 1])), (int(X[ptr, 2]), int(X[ptr, 3])), [0, 0, 0])
                        cv2.putText(images_cv[0, :, :, :], str(scores_after_merging[ptr][0])[:6], (int(X[ptr, 2]), int(X[ptr, 3])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0))

            # cv2.putText(images_cv[0, :, :, :], str(score)[:6], (int(X[ptr, 2] + 20), int(X[ptr, 3])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 200))

        cv2.imshow('img_orig', img_orig)
        cv2.imshow('image', (1.0 / 255.0) * images_cv[0, :, :, :])
        cv2.waitKey(1)

        # print comp_indicator
        print('frame idx: ', frame_idx)
        frame_idx += 1

    print('graph time: ', graph_time)
    print('cnn time: ', cnn_time)


if __name__ == '__main__':
    script_start_time = time.time()

    parser = argparse.ArgumentParser(description='Classification example - DIGITS')

    # Positional arguments
    parser.add_argument('caffemodel',   help='Path to a .caffemodel')
    parser.add_argument('deploy_file',  help='Path to the deploy file')
    parser.add_argument('video_file',
                        nargs='+',
                        help='Path[s] to an image')

    # Optional arguments

    parser.add_argument('-m', '--mean',
            help='Path to a mean binaryproto (*.binaryproto)')
    parser.add_argument('-npy_m', '--npy_mean',
                        help='Path to a mean file (*.npy)')
    parser.add_argument('-l', '--labels',
            help='Path to a labels file')
    parser.add_argument('--batch-size',
                        type=int)
    parser.add_argument('-c', '--conf',
                        help='Path to a json config  file (*.json)')

    parser.add_argument('-sufix',
                        type=str,
                        help="sufix to add to the results_file")
    parser.add_argument('-tracking_duration',
                        type=int,
                        help="tracking duration")

    parser.add_argument('-starting_frame',
                        type=int,
                        default=1,
                        help="frame to tstart from")

    args = vars(parser.parse_args())

    with open('configs.json') as json_data_file:
        json_parameters = json.load(json_data_file)

    print(json_parameters)
    print json_parameters['runtime']['gpu']
    print json_parameters['runtime']['use_gpu']

    detect(args['caffemodel'], args['deploy_file'], args['video_file'], args['mean'],
            bool(json_parameters['runtime']['use_gpu']), json_parameters['runtime']['gpu'], str( args['sufix']),
            args['tracking_duration'], json_parameters, args['starting_frame'],  args['labels'], args['batch_size'])

    print 'Script took %f seconds.' % (time.time() - script_start_time,)
