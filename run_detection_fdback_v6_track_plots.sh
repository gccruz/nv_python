#!/usr/bin/env bash

#MDL="../../../models/detectnet_v6/snapshot_iter_24045.caffemodel"
#DPL="../../../models/detectnet_v6/deploy_h10_e2.prototxt"
#MEAN="../../../models/detectnet_v6/mean.binaryproto"
#SFX="relive_submission_2"
MDL="../models_nv_python/detectnet_v6/snapshot_iter_24045.caffemodel"
DPL="../models_nv_python/detectnet_v6/deploy_h10_e2.prototxt"
MEAN="../models_nv_python/detectnet_v6/mean.binaryproto"
SFX="relive_submission_2"

#VDO="/home/gcx/datasets/videos_to_test/2015-04-22-16-05-15_jai_eo.avi"  # SUSP activity
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-28-36_jai_eo.avi"  # SUSP activity
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL $VDO -m $MEAN -tracking_duration 6 -sufix $SFX;

#
VDO="/home/gcx/datasets/videos_to_test/GP030175_part01.avi"
echo "Running $VDO$"
python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/bigShipHighAlt_clip2.avi"
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

#VDO="/media/gcx/1t/datasets/Sc1_Tk3/Sc1_Tk3_cam11.mp4"
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/media/gcx/1t/datasets/Sc1_Tk3/Sc1_Tk3_cam10.mp4"
#echo "Running $VDO$"
#python detect_fdback_skipper_track_plot.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;


# FIXME Traditional sequences used for TCSVT
#MDL="../../../models/detectnet_v6/snapshot_iter_24045.caffemodel"
#DPL="../../../models/detectnet_v6/deploy_h10_e2.prototxt"
#MEAN="../../../models/detectnet_v6/mean.binaryproto"
#SFX="v4_rect1_v5_5e3_mult"

# Traditional sequences used for TCSVT
#VDO="/home/gcx/gccruz_academiafa/DATASETS/seagull/datasets_portimao/first_exercise/2015-04-22-portimao/jai/boat/2015-04-22-16-28-36_jai_eo.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/GP030175_part01.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/bigShipHighAlt_clip2.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;
#
#VDO="/home/gcx/datasets/videos_to_test/lanchaArgos_clip3.avi"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;

#VDO="/home/gcx/gccruz_academiafa/workspace/CFT_OTB/data_seq/jai_eo_2.mp4"
#echo "Running $VDO$"
#python detect_mult_skipper.py $MDL $DPL  $VDO -m  $MEAN  -tracking_duration 6 -sufix $SFX;